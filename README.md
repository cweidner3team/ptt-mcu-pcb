# Push-to-Talk: Device PCB

This part of the project has not yet started.
Will update this later.

## Library Usage

I am currently using the following libraries

- Library Installed with KiCAD
- [KiCAD Github Library](https://github.com/KiCad/kicad-library)


## Current Schematic

- [PDF](current-sch.pdf)

![Current Schematic](current-sch.png)



