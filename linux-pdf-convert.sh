#!/bin/sh

# Convert the pdf to image for Bitbucket README
# This only supports one page

convert -density 80 current-sch.pdf -quality 90 current-sch.png
